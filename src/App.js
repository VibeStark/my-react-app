import React from 'react';

const App = () => (
  <div className="container">
    <h1>Hello, world!</h1>
  </div>
);

export default App;
